# Cheat Chrome plugin

## Setup instructions

1. Check out this repo
2. Rename `config.json.dist` to `config.json`
3. Maybe set some variables in said `config.json`
4. Run `npm install`
5. Run `npx webpack`

### In Chrome
1. Navigate to `More tools`
2. Click `Extensions`
3. `Load unpacked`
4. Navigate to folder where you checked out this repo

#### Fun stuff
Reloading the extension isn't needed as long as `npx webpack` is running. 
All changes are available as soon as you refresh the page or navigate to a different page.

## Configuration
Configuration is done in the `config.json` file.
### PushBullet
[PushBullet]('https://pushbullet.com') is used to send notifications to a mobile device when antiCheat is active. 

In settings set the `access_token` and device identifier `device_iden` of your device. 

Create your `access_token` in `Settings`/`Account`

Link a device by installing the app on your device and running the setup.

Get your `device_iden` by going to `Devices` and clicking your device. Your url is now https://www.pushbullet.com/#devices/%THIS_IS_YOUR_DEVICE_ID%

### CheatServer
[CheatServer]('https://git.hansgoed.nl/camorra-world/cheat-server) is the communications server that allows for omnidirectional communication between the plugin and the outside world.

In settings set the `host` location of the server. This should be a websocket location, so  `ws://hostname:1798`.
CheatServer runs on port 1798.