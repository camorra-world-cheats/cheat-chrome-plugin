document.querySelector('#purgeDatabase').addEventListener('click', function () {
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        chrome.tabs.sendMessage(tabs[0].id, JSON.stringify(
            {
                'action': 'purgeDb'
            }
        ));
    });
});

let actionButtons = document.querySelectorAll('.startAction');

for (let button of actionButtons) {
    button.addEventListener('click', function (event) {
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            chrome.tabs.sendMessage(tabs[0].id, JSON.stringify({
                'action': 'startCrime',
                'meta': {
                    'crime': event.target.getAttribute('data-crime')
                }
            }));
        });
    });
}
