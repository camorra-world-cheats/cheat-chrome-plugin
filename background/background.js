const requestFilter = {
    urls: [
        "https://www.camorraworld.nl/user/captcha/icon.php?hash=*"
    ],
    types: [
        "image"
    ]
};

// This is triggered when the extension is installed or updated.
chrome.runtime.onInstalled.addListener(function () {
    chrome.webRequest.onCompleted.addListener(function (request) {
        // TODO, shit, this version of chrome doesn't support getting response bodies yet..
        console.log(request);
    }, requestFilter);
});
