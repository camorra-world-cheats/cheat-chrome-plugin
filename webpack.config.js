const ChromeExtensionReloader  = require('webpack-chrome-extension-reloader');

module.exports = {
    entry: {
        background: './background/background.js',
        content: './src/index.js',
        popup: './popup/popup.js'
    },

    mode: "development",
    devtool: "source-map",
    watch: true,
    plugins: [
        new ChromeExtensionReloader({
            reloadPage: false,
            entries: {
                contentScript: 'content',
                background: 'background',
                popup: 'popup'
            }
        })
    ]
};