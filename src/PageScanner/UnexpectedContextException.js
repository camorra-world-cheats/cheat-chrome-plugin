export default class UnexpectedContextException extends Error {
    constructor(message) {
        super(message);
    }

}