import UnexpectedContextException from "./UnexpectedContextException";
import SimpleRobberySecurity from "../PageScans/Security/SimpleRobbery";
import HarderRobberySecurity from "../PageScans/Security/HarderRobbery";
import DrivingLessonSecurity from "../PageScans/Security/DrivingLesson";
import DrivingLessonInformation from "../PageScans/Information/DrivingLesson";
import LoggedOut from "../PageScans/Special/LoggedOut";
import GrandTheftAutoSecurity from "../PageScans/Security/GrandTheftAuto"
import GrandTheftAutoInformation from "../PageScans/Information/GrandTheftAuto";
import KillPracticeInformation from "../PageScans/Information/KillPractice";
import SimpleRobberyInformation from "../PageScans/Information/SimpleRobbery";
import HarderRobberyInformation from "../PageScans/Information/HarderRobbery";
import AntiCheatSecurity from "../PageScans/Security/AntiCheat";
import AntiCheatSpecial from "../PageScans/Special/AntiCheat";
import Jail from "../PageScans/Special/Jail";
import JailPreScan from "../PageScans/Special/JailPreScan";
import VillaScan from "../PageScans/Information/VillaScan.js";
import DrugPricesScan from "../PageScans/Information/DrugPricesScan.js";
import AntiCheatSolves from "../PageScans/Special/AntiCheatSolves";

export default class PageScanner {
    static scan(executionGroup) {
        let promises = [];
        try {
            promises.push(AntiCheatSecurity.scan(executionGroup));
            promises.push(JailPreScan.scan(executionGroup));
            promises.push(GrandTheftAutoSecurity.scan(executionGroup));
            promises.push(SimpleRobberySecurity.scan(executionGroup));
            promises.push(HarderRobberySecurity.scan(executionGroup));
            promises.push(DrivingLessonSecurity.scan(executionGroup));
            promises.push(LoggedOut.scan(executionGroup));
            promises.push(GrandTheftAutoInformation.scan(executionGroup));
            promises.push(SimpleRobberyInformation.scan(executionGroup));
            promises.push(HarderRobberyInformation.scan(executionGroup));
            promises.push(DrivingLessonInformation.scan(executionGroup));
            promises.push(KillPracticeInformation.scan(executionGroup));
            promises.push(VillaScan.scan());
            promises.push(DrugPricesScan.scan());
            promises.push(AntiCheatSolves.scan());
        } catch (e) {
            if (!(e instanceof UnexpectedContextException)) {
                throw e;
            }

            console.log(e.message);

            // Unexpected content. Could be jail?
            Jail.scan(executionGroup).then(function () {
                // If this promise resolves it's not jail..

                promises.push(AntiCheatSpecial.scan(executionGroup));
            });

            // Never ending promise should fix actions being executed during anticheat/jailtime
            promises.push(new Promise(function () {}));
        }

        return Promise.all(promises);
    }
}