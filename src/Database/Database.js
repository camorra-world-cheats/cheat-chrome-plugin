import PouchDB from 'pouchdb-browser';
import PouchDBFind from 'pouchdb-find';

export default class {
    static getTasksDatabase() {
        PouchDB.plugin(PouchDBFind);
        let database = new PouchDB('actions');

        return new Promise(function (resolve) {
            database.createIndex({
                index: {
                    fields: ['executionTime', 'createdAt']
                }
            }).then(function () {
                resolve(database);
            });
        });
    }
}