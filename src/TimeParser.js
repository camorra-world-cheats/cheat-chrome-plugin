export default class {
    static getSeconds(timeString) {
        if (timeString === 'Let\'s go!') {
            return 0;
        }

        let matches = timeString.match(/^(?:(\d+)*d )?(?:(\d+)*h )?(?:(\d+)*m )?(\d+)s$/);
        let days = matches[1] || 0;
        let hours = matches[2] || 0;
        let minutes = matches[3] || 0;
        let seconds = matches[4];

        return (days * 24 * 60 * 60) + (hours * 60 * 60) + (minutes * 60) + parseInt(seconds);
    }

    static getSecondsFromDateString(dateString) {
        let months = {
            'januari': 1,
            'februari': 2,
            'maart': 3,
            'april': 4,
            'mei': 5,
            'juni': 6,
            'juli': 7,
            'augustus': 8,
            'september': 9,
            'oktober': 10,
            'november': 11,
            'december': 12
        };

        let matches = dateString.match(/^(?:maandag|dinsdag|woensdag|donderdag|vrijdag|zaterdag|zondag) (\d+) (januari|februari|maart|april|mei|juni|juli|augustus|september|oktober|november|december) (\d{4}), (\d+):(\d{2})uur$/);
        let day = matches[1];
        let month = months[matches[2]];
        let year = matches[3];
        let hour = matches[4];
        let minute = matches[5];

        let date = new Date(year + '-' + month + '-' + day + ' ' + hour + ':' + minute);

        return (date.getTime() - Date.now()) / 1000;
    }

    static getSecondsFromUnformattedTimeString(timeString) {
        let time = timeString.split(':');

        let hours = parseInt(time[0]);
        let minutes = parseInt(time[1]);
        let seconds = parseInt(time[2]);

        return seconds + (minutes * 60) + (hours * 3600);
    }
}