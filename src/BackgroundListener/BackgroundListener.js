import PurgeDatabase from "./Actions/PurgeDatabase";
import Crimes from "../Crimes";
import PouchDB from "pouchdb-browser";

export default class {
    static start() {
        let database = this.getDatabase();
        chrome.runtime.onMessage.addListener(function(message) {
            console.log('message received: "' + message + '"');

            let decodedMessage = JSON.parse(message);

            if (decodedMessage.action === 'purgeDb') {
               PurgeDatabase.execute();
            }

            if (decodedMessage.action === 'startCrime') {
                switch (decodedMessage.meta.crime) {
                    case 'cokeRun':
                        Crimes.startNewCokeRun(0, database);
                        break;
                    case 'cokeSell':
                        Crimes.startNewSellDrugs('coke', 0, database);
                        break;
                    default:
                        alert('Crime "' + decodedMessage.meta.crime + '" is not configured');
                        break;
                }
            }

        });
    }

    static getDatabase() {
        return new PouchDB('actions');
    }
}
