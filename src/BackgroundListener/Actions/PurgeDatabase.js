import PouchDB from 'pouchdb-browser';
import Crimes from "../../Crimes";

export default class {
    static execute() {
        new PouchDB('actions').destroy().then(function () {
            console.log('database purged!');
        });

        window.localStorage.removeItem('currentTaskId');
    }
}