import PouchDB from 'pouchdb-browser';
import PouchdbFind from 'pouchdb-find';

PouchDB.plugin(PouchdbFind);

import TaskSelector from "./TaskSelector/TaskSelector";
import PageScanner from "./PageScanner/PageScanner";
import BackgroundListener from "./BackgroundListener/BackgroundListener";
import Database from "./Database/Database";

BackgroundListener.start();

(function() {
    start();
})();


function start() {
    Database.getTasksDatabase().then(function (database) {
        let currentTaskId = window.localStorage.getItem('currentTaskId');

        if (currentTaskId === null) {
            PageScanner.scan().then(function () {
                startNextTask(database);
            });
            return;
        }

        database.get(currentTaskId).then(function (currentTask) {
            if (currentTask === null) {
                window.localStorage.removeItem('currentTaskId');
                PageScanner.scan().then(function () {
                    startNextTask(database);
                });
                return;
            }

            let executionGroup = currentTask.executionGroup;
            PageScanner.scan(executionGroup).then(function () {
                window.localStorage.removeItem('currentTaskId');

                let action = TaskSelector.createItem(currentTask);
                if (!action.checkResult()) {
                    alert('expected something else!');
                    return 2;
                }

                database.remove(currentTask).then(function () {
                    startNextTask(database, executionGroup);
                });
            });

        });
    });
}

function startNextTask(database, executionGroup = null) {
    let taskSelector = new TaskSelector(database);
    taskSelector.getNext(executionGroup).then(function (nextTasks) {
        if (nextTasks.docs.length === 0) {
            if (executionGroup === null) {
                alert('No actions left over..');
                return;
            }

            startNextTask(database);
            return;
        }

        let nextTask = nextTasks.docs[0];
        let timeout = nextTask.executionTime - Date.now();
        if (timeout < 0) {
            timeout = 0;
        }

        // Prevent auto logout
        // TODO this can be a task
        if (timeout > 540000) {
            setTimeout(window.location.reload, 540000);
            return;
        }

        // Execute task when the time has come
        setTimeout(function () {
            taskSelector.execute(nextTask).then(function () {
                database.remove(nextTask).then(function () {
                    window.localStorage.removeItem('currentTaskId');
                    startNextTask(database);
                });
            });
        }, timeout);
    });
}