import Config from "../../config";
import WebSocketClient from 'isomorphic-ws';

export default class {
    constructor() {
        this.listeners = [];
    }

    openConnection() {
        let self = this;
        return new Promise(function(resolve) {
            let webSocket = new WebSocketClient(Config.cheatServer.host, 'chrome-extension-v1');

            webSocket.onopen = function () {
                self.webSocket = webSocket;
                resolve();
            };

            webSocket.onmessage = function (data) {
                for (let i in this.listeners) {
                    if (this.listeners.hasOwnProperty(i)) {
                        continue;
                    }

                    this.listeners[i](data);
                }
            };
        });
    }

    sendText(text) {
        this.webSocket.send(text);
    }

    addListener(callback) {
        this.listeners.push(callback);
    }
}
