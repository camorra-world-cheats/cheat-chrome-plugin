import Navigate from '../Actions/Generic/Navigate';
import TaskSelector from './TaskSelector'
import SubmitForm from "../Actions/Generic/SubmitForm";
import SelectOptionInDropdown from "../Actions/Generic/SelectOptionInDropdown";
import SelectRadio from "../Actions/Generic/SelectRadio";
import ClickButton from "../Actions/Generic/ClickButton";
import Bank from "../Actions/Specific/Bank";
import SetInputValue from "../Actions/Generic/SetInputValue";
import StoreNavigate from "../Actions/Specific/StoreNavigate";
import StoreBuy from "../Actions/Specific/StoreBuy";
import DrugsBuy from "../Actions/Specific/DrugsBuy";
import SettingsNavigate from "../Actions/Specific/SettingsNavigate";
import StealCar from "../Actions/Specific/StealCar";
import Robbery from "../Actions/Specific/Robbery";
import DrivingLesson from "../Actions/Specific/DrivingLesson";
import KillPractice from "../Actions/Specific/KillPractice";
import HospitalizeYourself from "../Actions/Specific/HospitalizeYourself";
import WaitTimesNavigate from "../Actions/Specific/WaitTimesNavigate";
import CokeRun from "../Actions/Specific/CokeRun";
import DrugsSell from "../Actions/Specific/DrugsSell";

export default class {
    constructor(database) {
        this.database = database;
    }

    getNext(executionGroup) {
        let selector = {
            executionTime: {$gt: null},
            createdAt: {$gt: null}

        };
        if (executionGroup !== null && typeof executionGroup !== 'undefined') {
            selector['executionGroup'] = {$eq: executionGroup}
        }

        return this.database.find({
            selector: selector,
            sort: [
                {'executionTime': 'asc'},
                {'createdAt': 'asc'}
            ]
        });
    }

    static createItem(task) {
        switch (task.name) {
            case 'Navigate':
                return Navigate.createFromTask(task);
            case 'SubmitForm':
                return SubmitForm.createFromTask(task);
            case 'SelectOptionInDropdown':
                return SelectOptionInDropdown.createFromTask(task);
            case 'SelectRadio':
                return SelectRadio.createFromTask(task);
            case 'ClickButton':
                return ClickButton.createFromTask(task);
            case 'Bank':
                return Bank.createFromTask(task);
            case 'SetInputValue':
                return SetInputValue.createFromTask(task);
            case 'StoreNavigate':
                return StoreNavigate.createFromTask(task);
            case 'StoreBuy':
                return StoreBuy.createFromTask(task);
            case 'DrugsBuy':
                return DrugsBuy.createFromTask(task);
            case 'DrugsSell':
                return DrugsSell.createFromTask(task);
            case 'SettingsNavigate':
                return SettingsNavigate.createFromTask(task);
            case 'StealCar':
                return StealCar.createFromTask(task);
            case 'Robbery':
                return Robbery.createFromTask(task);
            case 'DrivingLesson':
                return DrivingLesson.createFromTask(task);
            case 'KillPractice':
                return KillPractice.createFromTask(task);
            case 'HospitalizeYourself':
                return HospitalizeYourself.createFromTask(task);
            case 'WaitTimesNavigate':
                return WaitTimesNavigate.createFromTask(task);
            case 'CokeRun':
                return CokeRun.createFromTask(task);
            default:
                alert(task.name + ' is not a valid task');
        }
    }

    execute(task) {
        console.log('executing task: "' + task.name + '" with', task.meta);

        window.localStorage.setItem('currentTaskId', task._id);
        return TaskSelector.createItem(task).execute();
    }
};