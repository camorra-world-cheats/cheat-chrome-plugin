import Navigate from "./Actions/Generic/Navigate";
import StealCar from "./Actions/Specific/StealCar";
import Robbery from "./Actions/Specific/Robbery";
import WaitTimesNavigate from "./Actions/Specific/WaitTimesNavigate";
import SettingsNavigate from "./Actions/Specific/SettingsNavigate";
import DrivingLesson from "./Actions/Specific/DrivingLesson";
import ExecutionClearer from "./ExecutionClearer/ExecutionClearer";
import KillPractice from "./Actions/Specific/KillPractice";
import HospitalizeYourself from "./Actions/Specific/HospitalizeYourself";
import CokeRun from "./Actions/Specific/CokeRun";
import DrugsSell from "./Actions/Specific/DrugsSell";

export default class {
    static get DrivingLesson () {
        return 'driving_lesson';
    };

    static get GrandTheftAuto() {
        return 'grand_theft_auto';
    }

    static get Robbery() {
        return 'robbery';
    }

    static get CokeRun() {
        return 'coke_run';
    }

    static get KillPractice() {
        return 'kill_practice';
    }

    static startNewSimpleRobbery(time, database) {
        let promises = [];

        let previousExecutionGroup = window.localStorage.getItem('executionGroup_' + this.Robbery);
        promises.push(ExecutionClearer.removeExecutionsForExecutionGroup(previousExecutionGroup));

        let simpleRobberyNavigation = new Navigate('Simpele overval', '/crime/light.php', null, time);
        promises.push(simpleRobberyNavigation.save(database));

        let simpleRobbery = new Robbery(simpleRobberyNavigation.executionGroup, time);
        promises.push(simpleRobbery.save(database));

        window.localStorage.setItem('executionGroup_' + this.Robbery, simpleRobberyNavigation.executionGroup);
        return Promise.all(promises);
    }

    static startNewHarderRobbery(time, database) {
        let promises = [];

        let previousExecutionGroup = window.localStorage.getItem('executionGroup_' + this.Robbery);
        promises.push(ExecutionClearer.removeExecutionsForExecutionGroup(previousExecutionGroup));

        let harderRobberyNavigation = new Navigate('Moeilijkere overval', '/crime/medium.php', null, time);
        promises.push(harderRobberyNavigation.save(database));

        let harderRobbery = new Robbery(harderRobberyNavigation.executionGroup, time);
        promises.push(harderRobbery.save(database));

        window.localStorage.setItem('executionGroup_' + this.Robbery, harderRobberyNavigation.executionGroup);
        return Promise.all(promises);
    }

    static startNewDrivingLesson(type, time, database) {
        let promises = [];

        let previousExecutionGroup = window.localStorage.getItem('executionGroup_' + this.DrivingLesson);
        promises.push(ExecutionClearer.removeExecutionsForExecutionGroup(previousExecutionGroup));

        let drivingLessonNavigation = new Navigate('Illegale Rijles', '/crime/drivinglesson.php', null, time);
        promises.push(drivingLessonNavigation.save(database));

        let drivingLesson = new DrivingLesson(type, drivingLessonNavigation.executionGroup, time);
        promises.push(drivingLesson.save(database));

        window.localStorage.setItem('executionGroup_' + this.DrivingLesson, drivingLessonNavigation.executionGroup);
        return Promise.all(promises);
    }

    static startNewGrandTheftAuto(time, database) {
        let promises = [];

        let previousExecutionGroup = window.localStorage.getItem('executionGroup_' + this.GrandTheftAuto);
        promises.push(ExecutionClearer.removeExecutionsForExecutionGroup(previousExecutionGroup));

        let grandTheftAutoNavigation = new Navigate('Grand Theft Auto', '/crime/gta.php', null, time);
        promises.push(grandTheftAutoNavigation.save(database));

        let stealCar = new StealCar(grandTheftAutoNavigation.executionGroup, time);
        promises.push(stealCar.save(database));

        window.localStorage.setItem('executionGroup_' + this.GrandTheftAuto, grandTheftAutoNavigation.executionGroup);
        return Promise.all(promises);
    }

    static startNewCokeRun(time, database) {
        let promises = [];

        let previousExecutionGroup = window.localStorage.getItem('executionGroup_' + this.CokeRun);
        promises.push(ExecutionClearer.removeExecutionsForExecutionGroup(previousExecutionGroup));

        let statisticsNavigation = new Navigate('Statistieken', '/user/dashboard.php', null, time);
        promises.push(statisticsNavigation.save(database));

        let cokeFactoryNavigation = new WaitTimesNavigate('Coke runnen', '/business/coke-info.php', statisticsNavigation.executionGroup, time);
        promises.push(cokeFactoryNavigation.save(database));

        let cokeRun = new CokeRun(statisticsNavigation.executionGroup, time);
        promises.push(cokeRun.save(database));

        window.localStorage.setItem('executionGroup_' + this.CokeRun, statisticsNavigation.executionGroup);
        return Promise.all(promises);
    }

    static startNewKillPractice(time, database) {
        let promises = [];

        let previousExecutionGroup = window.localStorage.getItem('executionGroup_' + this.KillPractice);
        promises.push(ExecutionClearer.removeExecutionsForExecutionGroup(previousExecutionGroup));

        let killPracticeNavigation = new Navigate('Moord oefening', '/crime/killpractice.php', null, time);
        promises.push(killPracticeNavigation.save(database));

        let killPractice = new KillPractice(killPracticeNavigation.executionGroup, time);
        promises.push(killPractice.save(database));

        window.localStorage.setItem('executionGroup_' + this.KillPractice, killPracticeNavigation.executionGroup);
        return Promise.all(promises);
    }

    createNewBankAction() {
        // TODO this doesn't work at all. This is still in the old format, horrible copy paste job
        let promises = [];

        let navigateToBank = new Navigate('Bank Account', '/user/cash.php', this.executionGroup, Date().getTime() + 600000);
        promises.push(navigateToBank.save(database));

        let bank = new this(this.meta.targetAmount, this.meta.target, this.executionGroup);
        promises.push(bank.save(database));

        return Promise.all(promises);
    }

    static startNewHospitalAction(time, database) {
        let promises = [];

        let navigateToHospital = new Navigate('Ziekenhuis', '/cw/hospital.php', null, time);
        promises.push(navigateToHospital.save(database));

        let hospitalizeYourself = new HospitalizeYourself(navigateToHospital.executionGroup);
        promises.push(hospitalizeYourself.save(database));

        return Promise.all(promises);
    }

    static startNewSellDrugs(drug, time, database) {
        let promises = [];

        let navigateToDrugDealPage = new Navigate('Drugs dealen', '/crime/deal_drugs.php', null, time);
        promises.push(navigateToDrugDealPage.save(database));

        let sellDrugs = new DrugsSell(drug);
        promises.push(sellDrugs.save(database));

        return Promise.all(promises);
    }
}