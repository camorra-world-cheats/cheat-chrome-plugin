import Database from "../Database/Database";

export default class {
    static removeExecutionsForExecutionGroup(executionGroup) {
        let currentTaskId = window.localStorage.getItem('currentTaskId');

        if (executionGroup === null) {
            return Promise.resolve();
        }

        return Database.getTasksDatabase().then(function(database) {
            return database.find({
                selector: {
                    'executionGroup': {$eq: executionGroup}
                }
            }).then(function (results) {
                let promises = [];
                for (let i in results.docs) {
                    if (!results.docs.hasOwnProperty(i)) {
                        continue;
                    }

                    if (results.docs[i]._id === currentTaskId) {
                        continue;
                    }

                    promises.push(database.remove(results.docs[i]));
                }

                return Promise.all(promises);
            })
        });
    }
}