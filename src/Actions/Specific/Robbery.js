import UnexpectedContextException from "../../PageScanner/UnexpectedContextException";
import SelectRadio from "../Generic/SelectRadio";
import ClickButton from "../Generic/ClickButton";
import UuidV4 from "uuid/v4";
import PouchDB from "pouchdb-browser";

export default class {
    constructor(executionGroup = null, executionTime = 0) {
        this.name = 'Robbery';
        this.meta = {};
        this.executionTime = executionTime.toString();

        if (executionGroup === null) {
            executionGroup = UuidV4();
        }

        this.executionGroup = executionGroup;
    }

    save(database) {
        return database.post({
            name: this.name,
            meta: this.meta,
            executionTime: this.executionTime,
            executionGroup: this.executionGroup,
            createdAt: window.performance.now().toString()
        })
    }

    execute() {
        let radios = document.querySelectorAll('.cb form input[type="radio"]');
        if (radios.length !== 3) {
            throw new UnexpectedContextException("Simple robbery: More choices than expected");
        }

        let mostChance = -1;
        let mostChanceOption;

        for (let i in radios) {
            if (!radios.hasOwnProperty(i)) {
                continue;
            }

            let radio = radios[i];
            if (radio.labels.length !== 1) {
                throw new UnexpectedContextException("Simple robbery: Different amount of labels than expected");
            }

            let chanceText = radio.parentElement.innerText;

            let chance = chanceText.match(/^ (\d{1,3})% kans$/)[1];

            if (chance === null) {
                throw new UnexpectedContextException("Simple robbery: Chance isn't in expected format");
            }

            if (chance >= mostChance) {
                mostChanceOption = radio;
                mostChance = chance;
            }
        }

        let promises = [];

        let mostChanceSelectRadio = new SelectRadio('form', '#' + mostChanceOption.id);
        promises.push(mostChanceSelectRadio.save(this.getDatabase()));

        let clickSubmitButton = new ClickButton('.cb form input[type="submit"]', 0);
        promises.push(clickSubmitButton.save(this.getDatabase()));

        return Promise.all(promises);
    }

    checkResult() {
        let notifications = document.querySelectorAll('#notice_group .notice');

        return notifications.length !== 0;
    }

    getDatabase() {
        return new PouchDB('actions');
    }

    static createFromTask(task) {
        if (task.name !== 'Robbery') {
            alert('this is not a task that can become a Robbery item');
        }

        return new this(
            task.meta.item,
            task.meta.amount,
            task.executionGroup,
            task.executionTime
        );
    }
}