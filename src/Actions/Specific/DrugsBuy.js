import UuidV4 from 'uuid/v4';
import SelectOptionInDropdown from "../Generic/SelectOptionInDropdown";
import SetInputValue from "../Generic/SetInputValue";
import ClickButton from "../Generic/ClickButton";
import Database from "../../Database/Database";

export default class {
    constructor(item, amount, executionGroup = null, executionTime = 0) {
        this.name = 'DrugsBuy';
        this.meta = {
            item: item,
            amount: amount
        };
        this.executionTime = executionTime.toString();

        if (executionGroup === null) {
            executionGroup = UuidV4();
        }

        this.executionGroup = executionGroup;
    }

    save(database) {
        return database.post({
            name: this.name,
            meta: this.meta,
            executionTime: this.executionTime,
            executionGroup: this.executionGroup,
            createdAt: window.performance.now().toString()
        })
    }

    execute() {
        let itemToPurchase = this.meta.item;
        let amountToPurchase = this.meta.amount;

        let self = this;
        if (itemToPurchase === 'cheap') {
            let form = document.querySelector('.cb form');

            let drugSelect = form.querySelector('select[name="drug"]');

            let prices = {};
            for (let i in drugSelect.options) {
                if (!drugSelect.options.hasOwnProperty(i)) {
                    continue;
                }

                let option = drugSelect.options[i];
                if (option.innerText.match(/^Isolater hasj - € .*kg$/)) {
                    prices['hasj'] = parseInt(option.innerText.replace(/\D/g, ''));
                }

                if (option.innerText.match(/^Pure cocaine - € .*kg$/)) {
                    prices['coke'] = parseInt(option.innerText.replace(/\D/g, ''));
                }
            }

            if (prices['hasj'] < 11500) {
                let promises = [];

                return Database.getTasksDatabase().then(function (database) {
                    let setAmount = new SetInputValue('.cb form input#aantal', amountToPurchase, self.executionGroup);
                    promises.push(setAmount.save(database));

                    let finalizeSale = new ClickButton('.cb form input#submit', 0, self.executionGroup);
                    promises.push(finalizeSale.save(database));

                    return Promise.all(promises);
                });
            }

            if (prices['coke'] < 27500) {
                let promises = [];

                return Database.getTasksDatabase().then(function(database) {
                    let cokeSelect = new SelectOptionInDropdown(/^Pure cocaine - € .*kg$/, '.cb form select[name="drug"]');
                    promises.push(cokeSelect.save(database));

                    let setAmount = new SetInputValue('.cb form input#aantal', amountToPurchase, self.executionGroup);
                    promises.push(setAmount.save(database));

                    let finalizeSale = new ClickButton('.cb form input#submit', 0, self.executionGroup);
                    promises.push(finalizeSale.save(database));

                    return Promise.all(promises);
                });
            }
        }

        // TODO buy the rest
        alert('Couldn\'t find item to buy');
        return new Promise(function () {});
    }

    checkResult() {
        let notifications = document.querySelectorAll('#notice_group .notice');

        if (notifications.length === 0) {
            return false;
        }

        let successHeaders = notifications.querySelectorAll('.header_success');
        if (successHeaders.length === 0) {
            return false;
        }
    }

    static createFromTask(task) {
        if (task.name !== 'DrugsBuy') {
            alert('this is not a task that can become a DrugsBuy item');
        }

        return new this(
            task.meta.item,
            task.meta.amount,
            task.executionGroup,
            task.executionTime
        );
    }
};