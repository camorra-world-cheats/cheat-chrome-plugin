import UuidV4 from 'uuid/v4';

export default class {
    constructor(item, executionGroup = null, executionTime = 0) {
        this.name = 'StoreBuy';
        this.meta = {
            item: item
        };
        this.executionTime = executionTime.toString();

        if (executionGroup === null) {
            executionGroup = UuidV4();
        }

        this.executionGroup = executionGroup;
    }

    save(database) {
        return database.post({
            name: this.name,
            meta: this.meta,
            executionTime: this.executionTime,
            executionGroup: this.executionGroup,
            createdAt: window.performance.now().toString()
        })
    }

    execute() {
        let itemToPurchase = this.meta.item;

        let itemTables = document.querySelectorAll('.cb table table');
        for (let i in itemTables) {
            let currentTable = itemTables[i];
            let itemHeader = currentTable.querySelector('td.green.bold');
            if (itemHeader.innerText === itemToPurchase) {
                let purchaseLink = currentTable.querySelector('a');
                if (purchaseLink.innerText === 'Koop') {
                    purchaseLink.click();
                    return new Promise(function () {});
                }
            }
        }

        alert('Couldn\'t find item to buy');
        return new Promise(function () {});
    }

    checkResult() {
        let notifications = document.querySelectorAll('#notice_group .notice');

        if (notifications.length === 0) {
            return false;
        }

        let successHeaders = notifications.querySelectorAll('.header_success');
        if (successHeaders.length === 0) {
            return false;
        }


    }


    static createFromTask(task) {
        if (task.name !== 'StoreBuy') {
            alert('this is not a task that can become a StoreNavigate item');
        }

        return new this(
            task.meta.item,
            task.executionGroup,
            task.executionTime
        );
    }
};