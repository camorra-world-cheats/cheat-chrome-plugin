import UuidV4 from 'uuid/v4';

export default class {
    constructor(menuTitle, expectedLocation, executionGroup = null, executionTime = 0) {
        this.name = 'StoreNavigate';
        this.meta = {
            title: menuTitle,
            expectedLocation: expectedLocation
        };
        this.executionTime = executionTime.toString();

        if (executionGroup === null) {
            executionGroup = UuidV4();
        }

        this.executionGroup = executionGroup;
    }

    save(database) {
        return database.post({
            name: this.name,
            meta: this.meta,
            executionTime: this.executionTime,
            executionGroup: this.executionGroup,
            createdAt: window.performance.now().toString()
        })
    }

    execute() {
        let targetTitle = this.meta.title;

        let links = document.querySelectorAll('.cb #sublinks a');

        for (let i in links) {
            if (links[i].innerText === targetTitle) {
                links[i].click();
                return new Promise(function () {});
            }
        }

        alert("Couldn't find link..");

        return new Promise(function () {});
    }

    checkResult() {
        if (this.meta.expectedLocation !== window.location.pathname + window.location.search
            && this.meta.expectedLocation !== window.location.href) {
            return false;
        }

        return true;
    }

    static createFromTask(task) {
        if (task.name !== 'StoreNavigate') {
            alert('this is not a task that can become a StoreNavigate item');
        }

        return new this(
            task.meta.title,
            task.meta.expectedLocation,
            task.executionGroup,
            task.executionTime
        );
    }
};