import ClickButton from "../Generic/ClickButton";
import UuidV4 from "uuid/v4";
import PouchDB from "pouchdb-browser";

export default class {
    constructor(executionGroup = null, executionTime = 0) {
        this.name = 'KillPractice';
        this.meta = {};
        this.executionTime = executionTime.toString();

        if (executionGroup === null) {
            executionGroup = UuidV4();
        }

        this.executionGroup = executionGroup;
    }

    save(database) {
        return database.post({
            name: this.name,
            meta: this.meta,
            executionTime: this.executionTime,
            executionGroup: this.executionGroup,
            createdAt: window.performance.now().toString()
        })
    }

    execute() {
        let promises = [];

        let clickSubmitButton = new ClickButton('.cb form input[type="submit"]', 0, this.executionGroup);
        promises.push(clickSubmitButton.save(this.getDatabase()));

        return Promise.all(promises);
    }

    checkResult() {
        let notifications = document.querySelectorAll('#notice_group .notice');

        return notifications.length !== 0;
    }

    getDatabase() {
        return new PouchDB('actions');
    }

    static createFromTask(task) {
        if (task.name !== 'KillPractice') {
            alert('this is not a task that can become a KillPractice item');
        }

        return new this(
            task.meta.item,
            task.meta.amount,
            task.executionGroup,
            task.executionTime
        );
    }
}