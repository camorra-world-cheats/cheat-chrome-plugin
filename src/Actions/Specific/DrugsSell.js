import UuidV4 from 'uuid/v4';
import SelectOptionInDropdown from "../Generic/SelectOptionInDropdown";
import SetInputValue from "../Generic/SetInputValue";
import PouchDB from "pouchdb-browser";
import ClickButton from "../Generic/ClickButton";

export default class {
    constructor(item, executionGroup = null, executionTime = 0) {
        this.name = 'DrugsSell';
        this.meta = {
            item: item
        };
        this.executionTime = executionTime.toString();

        if (executionGroup === null) {
            executionGroup = UuidV4();
        }

        this.executionGroup = executionGroup;
    }

    save(database) {
        return database.post({
            name: this.name,
            meta: this.meta,
            executionTime: this.executionTime,
            executionGroup: this.executionGroup,
            createdAt: window.performance.now().toString()
        })
    }

    execute() {
        let promises = [];

        if (this.meta.item === 'coke') {
            let selectCoke = new SelectOptionInDropdown(/^Coke .* kg$/, '.cb form select#drug-select');
            promises.push(selectCoke.save(this.getDatabase()));
        }

        let finalizeSale = new ClickButton('.cb:nth-child(3) form input#submit', 0, this.executionGroup);
        promises.push(finalizeSale.save(this.getDatabase()));

        return Promise.all(promises);
    }

    checkResult() {
        let notifications = document.querySelectorAll('#notice_group .notice');

        if (notifications.length === 0) {
            return false;
        }

        let successHeaders = notifications.querySelectorAll('.header_success');
        if (successHeaders.length === 0) {
            return false;
        }
    }

    getDatabase() {
        return new PouchDB('actions');
    }

    static createFromTask(task) {
        if (task.name !== 'DrugsSell') {
            alert('this is not a task that can become a DrugsSell item');
        }

        return new this(
            task.meta.item,
            task.meta.amount,
            task.executionGroup,
            task.executionTime
        );
    }
};