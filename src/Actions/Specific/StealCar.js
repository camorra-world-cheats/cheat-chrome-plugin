import UuidV4 from 'uuid/v4';
import SelectRadio from "../Generic/SelectRadio";
import ClickButton from "../Generic/ClickButton";
import Database from "../../Database/Database";

export default class {
    constructor(executionGroup = null, executionTime = 0) {
        this.name = 'StealCar';
        this.meta = {};
        this.executionTime = executionTime.toString();

        if (executionGroup === null) {
            executionGroup = UuidV4();
        }

        this.executionGroup = executionGroup;
    }

    save(database) {
        return database.post({
            name: this.name,
            meta: this.meta,
            executionTime: this.executionTime,
            executionGroup: this.executionGroup,
            createdAt: window.performance.now().toString()
        })
    }

    execute() {
        let executionGroup = this.executionGroup;

        return Database.getTasksDatabase().then(function (database) {
            let promises = [];
            let clickStealFromTheStreet = new SelectRadio('#contentbar_main form', 'table input#type1', executionGroup);
            promises.push(clickStealFromTheStreet.save(database));

            let submitForm = new ClickButton('#contentbar_main form input[type="submit"]', 0, executionGroup);
            promises.push(submitForm.save(database));

            return Promise.all(promises);
        });
    }

    checkResult() {
        return true;
    }

    static createFromTask(task) {
        if (task.name !== 'StealCar') {
            alert('this is not a task that can become a StealCar item');
        }

        return new this(
            task.executionGroup,
            task.executionTime
        );
    }
};