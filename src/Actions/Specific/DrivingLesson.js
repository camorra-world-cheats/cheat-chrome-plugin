import ClickButton from "../Generic/ClickButton";
import UuidV4 from "uuid/v4";
import Database from "../../Database/Database";

export default class {
    constructor(type, executionGroup = null, executionTime = 0) {
        this.name = 'DrivingLesson';
        this.meta = {
            type: type
        };
        this.executionTime = executionTime.toString();

        if (executionGroup === null) {
            executionGroup = UuidV4();
        }

        this.executionGroup = executionGroup;
    }

    save(database) {
        return database.post({
            name: this.name,
            meta: this.meta,
            executionTime: this.executionTime,
            executionGroup: this.executionGroup,
            createdAt: window.performance.now().toString()
        })
    }

    execute() {
        let submitButtons = document.querySelectorAll('.cb form input[type="submit"]');

        let self = this;

        return Database.getTasksDatabase().then(function (database) {
            if (submitButtons.length === 1 || self.type === "standard") {
                let clickStandardLesson = new ClickButton('.cb form input[type="submit"]', 0);
                return clickStandardLesson.save(database);
            }

            let clickBingoButton = new ClickButton('.cb form input[type="submit"]', 1);
            return clickBingoButton.save(database);
        });
    }

    checkResult() {
        let notifications = document.querySelectorAll('#notice_group .notice');

        return notifications.length !== 0;
    }

    static createFromTask(task) {
        if (task.name !== 'DrivingLesson') {
            alert('this is not a task that can become a DrivingLesson item');
        }

        return new this(
            task.meta.type,
            task.executionGroup,
            task.executionTime
        );
    }
}