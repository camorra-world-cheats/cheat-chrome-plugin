import UuidV4 from "uuid/v4";
import SelectRadio from "../Generic/SelectRadio";
import SetInputValue from "../Generic/SetInputValue";
import ClickButton from "../Generic/ClickButton";
import Database from "../../Database/Database";

export default class {
    constructor(targetAmount, target, executionGroup = null, executionTime = 0) {
        this.name = 'Bank';
        this.meta = {
            targetAmount: targetAmount,
            target: target
        };
        this.executionTime = executionTime.toString();

        if (executionGroup === null) {
            executionGroup = UuidV4();
        }

        this.executionGroup = executionGroup;
    }

    save(database) {
        return database.post({
            name: this.name,
            meta: this.meta,
            executionTime: this.executionTime,
            executionGroup: this.executionGroup,
            createdAt: window.performance.now().toString()
        })
    }

    execute() {
        let amounts = document.querySelectorAll('.cb table td a');

        let currentCashAmount = parseInt(amounts[0].innerText.replace(/\D/g, ''));
        let currentBankAmount = parseInt(amounts[1].innerText.replace(/\D/g, ''));

        let amountToTransfer;
        if (this.meta.target === 'bank') {
            amountToTransfer = (this.meta.targetAmount - currentBankAmount) * -1;
        } else {
            amountToTransfer = this.meta.targetAmount - currentCashAmount;
        }

        let promises = [];

        let self = this;

        return Database.getTasksDatabase().then(function (database) {
            if (amountToTransfer >= 0) {
                if (currentBankAmount < amountToTransfer) {
                    return this.createNewBankAction();
                }
            } else {
                if (currentCashAmount < amountToTransfer * -1) {
                    return this.createNewBankAction();
                }
                let selectRadio = new SelectRadio('.cb.domargin', '#typestort', self.executionGroup);
                promises.push(selectRadio.save(database));
            }

            if (amountToTransfer === null) {
                return Promise.resolve();
            }

            let fillInAmount = new SetInputValue('.cb.domargin input[name="bedrag"]', Math.abs(amountToTransfer), self.executionGroup);
            promises.push(fillInAmount.save(database));

            let clickButton = new ClickButton('.cb.domargin input[type="submit"]', 0);
            promises.push(clickButton.save(database));

            return Promise.all(promises);
        });
    }

    checkResult() {
        let amounts = document.querySelectorAll('.cb table td a');

        let currentCashAmount = parseInt(amounts[0].innerText.replace(/\D/g, ''));
        let currentBankAmount = parseInt(amounts[1].innerText.replace(/\D/g, ''));

        if (this.meta.target === 'bank') {
            return currentBankAmount === this.meta.targetAmount
        }

        return currentCashAmount === this.meta.targetAmount;
    }

    static createFromTask(task) {
        if (task.name !== 'Bank') {
            alert('this is not a task that can become a Bank item');
        }

        return new this(
            task.meta.targetAmount,
            task.meta.target,
            task.executionGroup,
            task.executionTime
        );
    }
};