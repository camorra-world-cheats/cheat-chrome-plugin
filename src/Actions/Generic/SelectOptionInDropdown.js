import UuidV4 from 'uuid/v4';

export default class {
    constructor(targetText, dropdownQueryString, executionGroup = null, executionTime = 0) {
        this.name = 'SelectOptionInDropdown';
        this.meta = {
            targetText: targetText,
            dropdownQueryString: dropdownQueryString
        };
        this.executionTime = executionTime.toString();

        if (executionGroup === null) {
            executionGroup = UuidV4();
        }

        this.executionGroup = executionGroup;
    }

    save(database) {
        return database.post({
            name: this.name,
            meta: this.meta,
            executionTime: this.executionTime,
            executionGroup: this.executionGroup,
            createdAt: window.performance.now().toString()
        })
    }

    execute() {
        let dropdown = document.querySelector(this.meta.dropdownQueryString);

        let options = dropdown.querySelectorAll('option');
        for (let i in options) {
            let option = options[i];

            if (option.innerText.match(this.meta.targetText)) {
                dropdown.value = option.value;
                dropdown.dispatchEvent(new Event("change"));
                return Promise.resolve();
            }
        }
    }

    checkResult() {
        return true;
    }

    static createFromTask(task) {
        if (task.name !== 'SelectOptionInDropdown') {
            alert('this is not a task that can become a SelectOptionInDropdown item');
        }

        return new this(
            task.meta.targetText,
            task.meta.dropdownQueryString,
            task.executionGroup,
            task.executionTime
        );
    }
};