import UuidV4 from 'uuid/v4';

export default class {
    constructor(formQuerySelector, radioQuerySelector, executionGroup = null, executionTime = 0) {
        this.name = 'SelectRadio';
        this.meta = {
            inputQuerySelector: formQuerySelector,
            radioQuerySelector: radioQuerySelector
        };
        this.executionTime = executionTime.toString();

        if (executionGroup === null) {
            executionGroup = UuidV4();
        }

        this.executionGroup = executionGroup;
    }

    save(database) {
        return database.post({
            name: this.name,
            meta: this.meta,
            executionTime: this.executionTime,
            executionGroup: this.executionGroup,
            createdAt: window.performance.now().toString()
        })
    }

    execute() {
        let form = document.querySelector(this.meta.inputQuerySelector);

        let radio = form.querySelector(this.meta.radioQuerySelector);

        radio.click();

        // Allow execution of the next steps
        return Promise.resolve();
    }

    checkResult() {
        return true;
    }

    static createFromTask(task) {
        if (task.name !== 'SelectRadio') {
            alert('this is not a task that can become a SelectRadio item');
        }

        return new this(
            task.meta.inputQuerySelector,
            task.meta.radioQuerySelector,
            task.executionGroup,
            task.executionTime
        );
    }
};