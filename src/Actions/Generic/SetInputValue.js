import UuidV4 from 'uuid/v4';

export default class {
    constructor(inputQuerySelector, value, executionGroup = null, executionTime = 0) {
        this.name = 'SetInputValue';
        this.meta = {
            inputQuerySelector: inputQuerySelector,
            value: value
        };
        this.executionTime = executionTime.toString();

        if (executionGroup === null) {
            executionGroup = UuidV4();
        }

        this.executionGroup = executionGroup;
    }

    save(database) {
        return database.post({
            name: this.name,
            meta: this.meta,
            executionTime: this.executionTime,
            executionGroup: this.executionGroup,
            createdAt: window.performance.now().toString()
        })
    }

    execute() {
        let input = document.querySelector(this.meta.inputQuerySelector);

        input.value = this.meta.value;

        // Allow execution of the next steps
        return Promise.resolve();
    }

    checkResult() {
        return true;
    }

    static createFromTask(task) {
        if (task.name !== 'SetInputValue') {
            alert('this is not a task that can become a SetInputValue item');
        }

        return new this(
            task.meta.inputQuerySelector,
            task.meta.value,
            task.executionGroup,
            task.executionTime
        );
    }
};