import UuidV4 from "uuid/v4";

export default class {
    constructor(buttonQuerySelector, index, executionGroup = null, executionTime = 0) {
        this.name = 'ClickButton';
        this.meta = {
            inputQuerySelector: buttonQuerySelector,
            index: index
        };
        this.executionTime = executionTime.toString();

        if (executionGroup === null) {
            executionGroup = UuidV4();
        }

        this.executionGroup = executionGroup;
    }

    save(database) {
        return database.post({
            name: this.name,
            meta: this.meta,
            executionTime: this.executionTime,
            executionGroup: this.executionGroup,
            createdAt: window.performance.now().toString()
        })
    }

    execute() {
        let buttons = document.querySelectorAll(this.meta.inputQuerySelector);

        let submitButton = buttons[this.meta.index];
        submitButton.click();

        return new Promise(function () {});
    }

    checkResult() {
        return true;
    }

    static createFromTask(task) {
        if (task.name !== 'ClickButton') {
            alert('this is not a task that can become a ClickButton item');
        }

        return new this(
            task.meta.inputQuerySelector,
            task.meta.index,
            task.executionGroup,
            task.executionTime
        );
    }
};