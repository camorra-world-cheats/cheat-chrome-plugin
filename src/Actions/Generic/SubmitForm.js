import UuidV4 from "uuid/v4";

export default class {
    constructor(formQuerySelector, executionGroup = null, executionTime = 0) {
        this.name = 'SubmitForm';
        this.meta = {
            inputQuerySelector: formQuerySelector
        };
        this.executionTime = executionTime.toString();

        if (executionGroup === null) {
            executionGroup = UuidV4();
        }

        this.executionGroup = executionGroup;
    }

    save(database) {
        return database.post({
            name: this.name,
            meta: this.meta,
            executionTime: this.executionTime,
            executionGroup: this.executionGroup,
            createdAt: window.performance.now().toString()
        })
    }

    execute() {
        let form = document.querySelector(this.meta.inputQuerySelector);

        let submitButton = form.querySelector('');
        submitButton.click();

        return new Promise(function () {});
    }

    checkResult() {
        return true;
    }

    static createFromTask(task) {
        if (task.name !== 'SubmitForm') {
            alert('this is not a task that can become a SubmitForm item');
        }

        return new this(
            task.meta.inputQuerySelector,
            task.executionGroup,
            task.executionTime
        );
    }
};