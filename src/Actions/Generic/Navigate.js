import UuidV4 from 'uuid/v4';

export default class {
    constructor(menuTitle, expectedLocation, executionGroup = null, executionTime = 0) {
        this.name = 'Navigate';
        this.meta = {
            title: menuTitle,
            expectedLocation: expectedLocation
        };
        this.executionTime = executionTime.toString();

        if (executionGroup === null) {
            executionGroup = UuidV4();
        }

        this.executionGroup = executionGroup;
    }

    save(database) {
        return database.post({
            name: this.name,
            meta: this.meta,
            executionTime: this.executionTime,
            executionGroup: this.executionGroup,
            createdAt: window.performance.now().toString()
        })
    }

    execute() {
        let targetTitle = this.meta.title;

        if (targetTitle === 'Help') {
            document.querySelector('#hu a').click();
            return new Promise(function () {});
        }

        if (targetTitle === 'Promotie') {
            document.querySelector('#promotebutton a').click();
            return new Promise(function () {});
        }

        if (targetTitle === 'Persoonlijk') {
            document.querySelector('#menuitems .pers a').click();
            return new Promise(function () {});
        }

        let links = document.querySelectorAll('.menu_items a');

        for (let i in links) {
            if (links[i].innerText === targetTitle) {
                links[i].click();
                return new Promise(function () {});
            }
        }

        let topMenu = document.querySelector('#menuitems');
        let personalLinks = topMenu.querySelectorAll('.pers a');
        for (let i in personalLinks) {
            if (personalLinks[i].innerText === targetTitle) {

                personalLinks[i].click();
                return new Promise(function () {});
            }
        }

        let countryLinks = topMenu.querySelectorAll('li.gamescene ul.menu-bar-items li a');

        for (let i in countryLinks) {
            if (countryLinks[i].innerText === targetTitle) {
                countryLinks[i].click();
                return new Promise(function () {});
            }
        }

        let intranetLinks = topMenu.querySelectorAll('.intra a');

        for (let i in intranetLinks) {
            if (intranetLinks[i].innerText === targetTitle) {
                intranetLinks[i].click();
                return new Promise(function () {});
            }
        }
        // TODO implement this for links in the intranet category
        alert("Couldn't find link..");
    }

    checkResult() {
        if (this.meta.expectedLocation !== window.location.pathname + window.location.search
            && this.meta.expectedLocation !== window.location.href) {
            return false;
        }

        return true;
    }

    static createFromTask(task) {
        if (task.name !== 'Navigate') {
            alert('this is not a task that can become a Navigation item');
        }

        return new this(
            task.meta.title,
            task.meta.expectedLocation,
            task.executionGroup,
            task.executionTime
        );
    }
};