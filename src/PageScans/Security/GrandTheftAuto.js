import UnexpectedContextException from "../../PageScanner/UnexpectedContextException";

export default class {
    static scan(executionGroup) {
        if (window.location.pathname !== '/crime/gta.php') {
            return Promise.resolve();
        }

        let successHeader = document.querySelector('.cb table tbody tr td span.tekstheader');
        if (successHeader !== null && successHeader.innerText === 'Je hebt met succes een auto gejat!') {
            return Promise.resolve();
        }

        let forms = document.querySelectorAll('#contentbar_main form');
        if (forms.length !== 1 && forms.length !== 2) {
            throw new UnexpectedContextException("Grand Theft Auto: Amount of forms doesn't match");
        }

        let headers = document.querySelectorAll('.cb h1');

        if (headers[0].innerText === 'Je moet nog wachten met Grand Theft Auto!') {
            return Promise.resolve();
        }

        if (headers[0].innerText !== 'Grand Theft Auto') {
            throw new UnexpectedContextException("Grand Theft Auto: Page title doesn't match");
        }
        if (headers[1].innerText !== 'Grand Theft Auto Ultra') {
            throw new UnexpectedContextException("Grand Theft Auto: Ultra title doesn't match");
        }

        // TODO this needs better validation
        let submitButtons = document.querySelectorAll('#contentbar_main form input[type="submit"]');
        if (submitButtons.length !== 1 && submitButtons.length !== 2) {
            throw new UnexpectedContextException("Grand Theft Auto: Amount of submit buttons isn't 1 or 2");
        }

        let radio = document.querySelector('#contentbar_main form table input[type="radio"]');
        if (radio.labels.length !== 1) {
            throw new UnexpectedContextException("Grand Theft Auto: Got a different amount of labels than expected");
        }

        if (radio.labels[0].innerText !== 'Jat auto van de straat') {
            throw new UnexpectedContextException("Grand Theft Auto: Steal from the street should be the first item on page");
        }

        if (radio.id !== 'type1') {
            throw new UnexpectedContextException("Grand Theft Auto: Steal from the street should have ID #type1");
        }
    }
}