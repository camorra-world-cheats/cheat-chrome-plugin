import UnexpectedContextException from "../../PageScanner/UnexpectedContextException";

export default class {
    static scan(executionGroup) {
        if (window.location.pathname !== '/crime/medium.php') {
            return;
        }

        let header = document.querySelector('#contentbar_main .cb:nth-child(2) h1');
        if (header === null) {
            throw new UnexpectedContextException("Harder robbery: Page doesn't have a title in the expected place");
        }

        if (header.innerText === 'Moeilijkere Overval - je rust nog uit...') {
            return;
        }

        if (document.querySelector('#contentbar_main .cb:nth-child(2) h1').innerText !== 'Moeilijkere overval - Train je skills!') {
            throw new UnexpectedContextException("Harder robbery: Page title doesn't match");
        }

        let forms = document.querySelectorAll('#contentbar_main form');
        if (forms.length !== 1) {
            throw new UnexpectedContextException("Harder robbery: Wrong number of forms");
        }

        let radios = forms[0].querySelectorAll('input[type="radio"]');
        if (radios.length !== 3) {
            throw new UnexpectedContextException("Harder robbery: More choices than expected");
        }

        let knownLabels = [
            'Beroof een restaurant na je maaltijd',
            'Beroof een pretpark',
            'Beroof een casino'
        ];

        for (let radio of radios) {
            for (let label of radio.labels) {
                if (knownLabels.indexOf(label.innerText) === -1) {
                    throw new UnexpectedContextException("Harder robbery: Label doesn't match one of the known labels");
                }
            }
        }

        return Promise.resolve();
    }
}