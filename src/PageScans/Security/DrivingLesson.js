import UnexpectedContextException from "../../PageScanner/UnexpectedContextException";
import PouchDB from "pouchdb-browser";

export default class {
    static scan(executionGroup) {
        if (!(window.location.pathname === '/crime/drivinglesson.php')) {
            return;
        }

        if (document.querySelector('#contentbar_main').children.length !== 2){
            throw new UnexpectedContextException("The amount of elements in the middle pane is not correct");
        }

        // CBS scanning
        let cbs = document.querySelector('#contentbar_main #cbs:nth-child(2)');

        if (cbs === null) {
            throw new UnexpectedContextException("CBS element does not exist or is not the second element in the middle pane");
        }

        if (cbs.children.length !== 2) {
            if (
                cbs.children.length !== 3 ||
                cbs.children[2].children.length !== 2 ||
                cbs.children[2].children[0].innerText !== 'Illegale Rijles - je wacht nog...')
            {
                throw new UnexpectedContextException("The amount of elements in the content pane is not correct");
            }
        }

        if (document.querySelector('#contentbar_main .cb:nth-child(2) h1').innerText === 'De rijinstructeur/instructrice is nog afgepeigerd!') {
            return;
        }

        if (document.querySelector('#contentbar_main .cb:nth-child(2) h1').innerText !== 'Illegale rijles in Camorra World') {
            throw new UnexpectedContextException("Driving lesson: Page title doesn't match");
        }

        let forms = document.querySelectorAll('#contentbar_main form');

        if (forms.length !== 1) {
            throw new UnexpectedContextException("Amount of forms is wrong");
        }

        let submitButtons = document.querySelectorAll('.cb form input[type="submit"]');

        if (submitButtons.length !== 1 && submitButtons.length !== 2) {
            throw new UnexpectedContextException("Driving lesson: Amount of choices is a bit off");
        }

        if (submitButtons.length === 2) {
            if (submitButtons[1].value !== 'Bingo Button!') {
                throw new UnexpectedContextException("Driving lesson: Second button is not the bingo button");
            }
        }

        if (submitButtons.length === 1) {
            if (submitButtons[0].value !== 'Neem rijles') {
                throw new UnexpectedContextException("Driving lesson: Only button is not the take lesson option");
            }
        }
    }

    static getDatabase() {
        return new PouchDB('actions');
    }
}