import PouchDB from "pouchdb-browser";
import TimeParser from "../../TimeParser";
import Crimes from "../../Crimes";

export default class {
    static scan(executionGroup) {
        if (!(window.location.pathname === '/crime/killpractice.php')) {
            return;
        }

        let promises = [];
        let notifications = document.querySelectorAll('#notice_group .notice');

        if (notifications.length > 0) {
            for (let notification of notifications) {
                if (notification.innerText.match(/Er gingen bij het afronden nog wat meer dingen mis, nu heb je een operatie nodig\.\.\.!/)) {
                    promises.push(Crimes.startNewHospitalAction(0, this.getDatabase()));
                }
            }
        }

        if (document.querySelector('#contentbar_main .cb:nth-child(2) h1').innerText === 'Wacht even met oefenen...') {
            let countDownElement = document.querySelector('#countdownSingle');
            if (countDownElement === null) {
                countDownElement = document.querySelector('.cb input[name="countdownnonvip"]');
            }
            let timeLeftString = countDownElement.value;
            let secondsLeft = TimeParser.getSeconds(timeLeftString);

            promises.push(Crimes.startNewKillPractice(secondsLeft * 1000 + new Date().getTime(), this.getDatabase()));
        }

        return Promise.all(promises);
    }

    static getDatabase() {
        return new PouchDB('actions');
    }
}