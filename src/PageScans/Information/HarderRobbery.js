import TimeParser from "../../TimeParser";
import Crimes from "../../Crimes";
import PouchDB from "pouchdb-browser";

export default class {
    static scan(executionGroup) {
        if (window.location.pathname !== '/crime/medium.php') {
            return;
        }

        let header = document.querySelector('#contentbar_main .cb:nth-child(2) h1');
        if (header.innerText === 'Moeilijkere Overval - je rust nog uit...') {
            let countDownElement = document.querySelector('#countdownsteel');
            if (countDownElement === null) {
                countDownElement = document.querySelector('.cb input[name="countdownnonvip"]');
            }

            let timeLeftString = countDownElement.value;
            let secondsLeft = TimeParser.getSeconds(timeLeftString);

            let database = this.getDatabase();

            return Crimes.startNewHarderRobbery(secondsLeft * 1000 + new Date().getTime(), database);
        }
    }

    static getDatabase() {
        return new PouchDB('actions');
    }
}