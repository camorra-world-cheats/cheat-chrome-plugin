import PouchDB from "pouchdb-browser";

export default class {
    static scan(executionGroup) {
        if (window.location.pathname === '/user/dashboard.php') {
            this.extractVillaInformationFromStatistics();
        }

        if (window.location.pathname === '/crime/deal_drugs.php') {
            this.extractVillaInformationFromDrugs()
        }
    }

    static getDatabase() {
        return new PouchDB('actions');
    }

    static extractVillaInformationFromStatistics() {
        let rows = document.querySelectorAll('.cb:nth-child(4) table tr');

        let villas = [];
        for (let row of rows) {
            if (row.querySelector('td:nth-child(1)').innerText === 'Woning') {
                villas.push(row.querySelector('td:nth-child(3)').innerText);
            }
        }

        localStorage.setItem('villas', JSON.stringify(villas));
    }

    static extractVillaInformationFromDrugs() {
        let rows = document.querySelectorAll('.cb:nth-child(4) table tr:nth-child(n+2)');

        let villas = [];
        for (let row of rows) {
            if (row.innerHTML === '') {
                // What an idiot uses empty rows as spacing..
                continue;
            }

            villas.push(row.querySelector('td:first-child').innerText)
        }

        localStorage.setItem('villas', JSON.stringify(villas));
    }
}