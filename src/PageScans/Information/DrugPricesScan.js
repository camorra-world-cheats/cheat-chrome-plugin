import PouchDB from "pouchdb-browser";
import TimeParser from "../../TimeParser";

export default class {
    static scan(executionGroup) {
        if (window.location.pathname + window.location.search !== '/intranet/stats/?type=drugweapon') {
            return;
        }

        let statisticsTable = document.querySelector('#STATcont');
        let rows = statisticsTable.querySelectorAll('tr:nth-child(n+2)');

        let villas = localStorage.getItem('villas');
        if (villas === null) {
            return;
        }

        let drugPrices = {};

        for (let row of rows) {
            let country = row.querySelector('td:nth-child(1)').innerText;

            if (villas.indexOf(country) === -1) {
                continue;
            }

            let hasjPrice = parseInt(row.querySelector('td:nth-child(5)').innerText.replace(/\D/g, ''));
            let cokePrice = parseInt(row.querySelector('td:nth-child(7)').innerText.replace(/\D/g, ''));

            drugPrices[country] = {'hasj': hasjPrice, 'coke': cokePrice}
        }

        drugPrices['expiry'] = this.getExpiryTime();
        localStorage.setItem('drugPrices', JSON.stringify(drugPrices));
    }

    static getDatabase() {
        return new PouchDB('actions');
    }

    static getExpiryTime() {
        let time = document.querySelector('.cb:nth-child(2) span');

        let secondsToGo = TimeParser.getSecondsFromUnformattedTimeString(time.innerText);

        return Date.now() + (secondsToGo * 1000)
    }
}