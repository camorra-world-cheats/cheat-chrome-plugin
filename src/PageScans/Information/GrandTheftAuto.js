import PouchDB from "pouchdb-browser";
import PouchDBFind from 'pouchdb-find';
import TimeParser from "../../TimeParser";
import Crimes from "../../Crimes";

export default class {
    static scan(executionGroup) {
        if (window.location.pathname !== '/crime/gta.php') {
            return Promise.resolve();
        }

        let successHeader = document.querySelector('.cb table tbody tr td span.tekstheader');
        if (successHeader !== null && successHeader.innerText === 'Je hebt met succes een auto gejat!') {
            let time = 30 * 1000 + new Date().getTime();
            return Crimes.startNewGrandTheftAuto(time, this.getDatabase());
        }

        let headers = document.querySelectorAll('.cb h1');
        if (headers[0].innerText === 'Je moet nog wachten met Grand Theft Auto!') {
            let promises = [];

            let countDownElement = document.querySelector('#countdownauto');
            if (countDownElement === null) {
                countDownElement = document.querySelector('.cb input[name="countdownnonvip"]');
            }

            let timeLeftString = countDownElement.value;
            let secondsLeft = TimeParser.getSeconds(timeLeftString);

            let time = secondsLeft * 1000 + new Date().getTime();
            promises.push(Crimes.startNewGrandTheftAuto(time, this.getDatabase()));

            return Promise.all(promises);
        }
    }

    static getDatabase() {
        PouchDB.plugin(PouchDBFind);
        return new PouchDB('actions');
    }
}