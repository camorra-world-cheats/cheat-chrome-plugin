import UnexpectedContextException from "../../PageScanner/UnexpectedContextException";
import PouchDB from "pouchdb-browser";
import TimeParser from "../../TimeParser";
import Crimes from "../../Crimes";
import ExecutionClearer from "../../ExecutionClearer/ExecutionClearer";

export default class {
    static scan(executionGroup) {
        if (!(window.location.pathname === '/crime/drivinglesson.php')) {
            return;
        }

        if (document.querySelector('#contentbar_main .cb:nth-child(2) h1').innerText === 'De rijinstructeur/instructrice is nog afgepeigerd!') {
            let countDownElement = document.querySelector('#countdownRi');
            if (countDownElement === null) {
                countDownElement = document.querySelector('.cb input[name="countdownnonvip"]');
            }
            let timeLeftString = countDownElement.value;
            let secondsLeft = TimeParser.getSeconds(timeLeftString);

            return Crimes.startNewDrivingLesson("bingo", secondsLeft * 1000 + new Date().getTime(), this.getDatabase());
        }

        let possiblePrices = [
            '€ 25 per les',
            '€ 500 per les',
            '€ 5.000 per les'
        ];

        if (possiblePrices.indexOf(document.querySelector('form p:last-child strong:first-child').innerText) === -1) {
            console.log('Not taking any driving lessons anymore');

            let previousExecutionGroup = window.localStorage.getItem('executionGroup_' + this.DrivingLesson);
            return ExecutionClearer.removeExecutionsForExecutionGroup(previousExecutionGroup);
        }
    }

    static getDatabase() {
        return new PouchDB('actions');
    }
}