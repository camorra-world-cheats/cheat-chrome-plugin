import PouchDB from "pouchdb-browser";

export default class {
    static scan(executionGroup) {
        let loginForm = document.querySelector('#loginform');
        if (loginForm === null) {
            return;
        }

       return this.getDatabase().destroy();
    }

    static getDatabase() {
        return new PouchDB('actions');
    }
}