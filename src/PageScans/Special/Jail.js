import TimeParser from "../../TimeParser";

export default class {
    static scan(executionGroup){
        let headers = document.querySelectorAll('.cb h1');
        if (headers.length !== 1) {
            return Promise.resolve();
        }

        if (headers[0].innerText !== 'Je zit in de gevangenis!') {
            console.log(headers[0].innerText);
            return Promise.resolve();
        }

        let timer = document.querySelector('span#timer1');

        let secondsLeft = TimeParser.getSecondsFromUnformattedTimeString(timer.innerText);

        if (secondsLeft > 10) {
            secondsLeft = 10;
        }

        setTimeout(function () {
            window.location.reload();
        }, secondsLeft * 1000);

        return new Promise(function () {});
    }
}