import UnexpectedContextException from "../../PageScanner/UnexpectedContextException";

export default class {
    static scan(executionGroup){
        let headers = document.querySelectorAll('.cb h1');
        if (headers.length !== 1) {
            return Promise.resolve();
        }

        if (headers[0].innerText === 'Je zit in de gevangenis!') {
            throw new UnexpectedContextException('You are in jail');
        }

        return Promise.resolve();
    }
}