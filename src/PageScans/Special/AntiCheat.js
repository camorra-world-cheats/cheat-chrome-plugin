import PushBullet from '../../PushBullet/Client';
import CheatServer from '../../CheatServer/Client'
import Uuidv4 from 'uuid/v4';

export default class {
    static scan(executionGroup) {
        let promises = [];

        promises.push(this.sendPushNotification());

        // promises.push(this.sendChallengeToCheatService());

        return Promise.all(promises);
    }

    static sendPushNotification() {
        let pushBulletClient = new PushBullet();

        return pushBulletClient.pushMessageToAllDevices('Camorra World Cheat', 'AntiCheat is active. Manually solve the challenge..');
    }

    static sendChallengeToCheatService() {
        let challenge = this.createChallengeObject();
        let cheatServer = new CheatServer();

        let promises = [];
        cheatServer.openConnection().then(function () {
            promises.push(cheatServer.sendText(JSON.stringify(challenge)));
        });

        cheatServer.addListener(function (message) {
            alert(message);
        });

        return Promise.all(promises);
    }

    static createChallengeObject() {
        let inputContainer = document.querySelector('#HumanValidationInputContainer');
        let image = inputContainer.querySelector('img');

        let imageDataUri = this.createImageDataUriFromImage(image);

        let challengeObject = {
            uuid: Uuidv4(),
            image: imageDataUri,
        };

        let options = inputContainer.querySelectorAll('input[type="radio"]');
        if (options.length !== 0) {
            challengeObject.type = 'objectRecognition';

            let choices = [];
            for (let i in options) {
                if (!options.hasOwnProperty(i)) {
                    continue;
                }

                choices.push(options[i].labels[0].innerText);
            }

            challengeObject.choices = choices;

            return challengeObject;
        }

        challengeObject.type = 'captcha';
        return challengeObject;
    }

    static createImageDataUriFromImage(image) {
        let canvas = document.createElement('canvas');
        let context = canvas.getContext('2d');
        canvas.height = image.naturalHeight;
        canvas.width = image.naturalWidth;
        context.drawImage(image, 0, 0);
        return canvas.toDataURL('image/png');
    }
}