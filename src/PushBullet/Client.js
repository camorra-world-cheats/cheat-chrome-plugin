import Axios from "axios";
import UuidV4 from 'uuid/v4';
import Settings from "../../config";

export default class {
    constructor() {
        this.client = Axios.create({
            baseURL: 'https://api.pushbullet.com/v2/',
            headers: {'Access-Token': Settings.pushBullet.access_token }
        });
    }

    getMe() {
        return this.client.get('users/me').then(function (result) {
            console.log(result);
        });
    }

    getDevices() {
        return this.client.get('devices');
    }

    pushMessageToAllDevices(title, body) {
        let promises = [];

        promises.push(this.client.post('pushes', {
            'type': 'note',
            'title': title,
            'body': body,
            'device_iden': Settings.pushBullet.device_iden,
            'guid': UuidV4()
        }));

        return Promise.all(promises);
    }
}